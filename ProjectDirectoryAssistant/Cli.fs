module ProjectDirectoryAssistant.Cli

open Argu

type PdaCdArguments =
    | [<MainCommand>] Dir of directory:string

    interface IArgParserTemplate with
        member this.Usage =
            match this with
            | Dir _ -> "Change directory to the indicated location"
and PdaArguments =
    | Cd of ParseResults<PdaCdArguments>