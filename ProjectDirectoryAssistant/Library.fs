﻿module ProjectDirectoryAssistant.Library

open System
open System.IO
open Legivel.Serialization



let parseConfig () =
    let homeDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
    let file = File.ReadAllText(homeDirectory +/ "pda.yml")
//    let file = File.ReadAllText (Directory.GetCurrentDirectory() +/ "../../../.." +/ "SampleConfigFile.yml")
    let config = Deserialize<ConfigurationFileData> file
    config
