module ProjectDirectoryAssistant.Utils

open System.IO

let (+/) path1 path2 = Path.Combine(path1, path2)