module ProjectDirectoryAssistant.Types

type Subdirectory = {
    key: string
    matcher: string list
}

type SubdirectoryPreset = {
    subDirectories: Subdirectory list
}

type Alias = {
    name: string
    aliases: string list
    path: string
    format: SubdirectoryPreset
}

type ConfigurationFileData = {
    //formats: SubdirectoryPreset list
    aliases: Alias list
}